FROM alpine:3.10

ARG VCS_REF
ARG BUILD_DATE

# Metadata
LABEL org.label-schema.vcs-ref=$VCS_REF \
      org.label-schema.name="cloud-utils" \
      org.label-schema.url="https://hub.docker.com/r/punkupoz/cloud-utils/" \
      org.label-schema.vcs-url="https://github.com/punkupoz/cloud-utils" \
      org.label-schema.build-date=$BUILD_DATE

ENV PATH usr/local/bin:/google-cloud-sdk/bin:$PATH

# Note: Latest version of kubectl may be found at:
# https://github.com/kubernetes/kubernetes/releases
ENV KUBE_LATEST_VERSION="v1.16.2"
# Note: Latest version of helm may be found at:
# https://github.com/kubernetes/helm/releases
ENV HELM_VERSION="v3.0.0"
# NOTE: Latest version of gcloud sdk may be found at:
# https://cloud.google.com/sdk/docs/downloads-versioned-archives
ENV CLOUD_SDK_VERSION="271.0.0"
# NOTE: Latest version of gcloud sdk may be found at:
# https://www.terraform.io/downloads.html
ENV TERRAFORM_VERSION="0.12.16"

RUN apk add --no-cache ca-certificates bash git openssh curl python py-pip \
    && apk add --virtual=build gcc libffi-dev musl-dev openssl-dev python-dev make \
    && pip install --upgrade pip \
    && wget -q https://storage.googleapis.com/kubernetes-release/release/${KUBE_LATEST_VERSION}/bin/linux/amd64/kubectl -O /usr/local/bin/kubectl \
    && chmod +x /usr/local/bin/kubectl \
    && wget -q https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz -O - | tar -xzO linux-amd64/helm > /usr/local/bin/helm \
    && chmod +x /usr/local/bin/helm \
    && wget https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-${CLOUD_SDK_VERSION}-linux-x86_64.tar.gz \
    && tar xzf google-cloud-sdk-${CLOUD_SDK_VERSION}-linux-x86_64.tar.gz \
    && rm google-cloud-sdk-${CLOUD_SDK_VERSION}-linux-x86_64.tar.gz \
    && ln -s /lib /lib64 \
    && gcloud config set core/disable_usage_reporting true \
    && gcloud --version \
    && pip --no-cache-dir install --upgrade awscli \
    && aws --version \
    && pip --no-cache-dir install azure-cli \
    && apk del --purge build \
    && pip --no-cache-dir install --upgrade azure-cli \
    && az --version \
    && apk del gcc libffi-dev musl-dev openssl-dev python-dev make \
    && wget -q https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip -O terraform.zip \
    && unzip terraform.zip && rm terraform.zip && mv terraform /usr/local/bin/terraform \
    && chmod +x /usr/local/bin/terraform \
    && curl -Lo skaffold https://storage.googleapis.com/skaffold/releases/latest/skaffold-linux-amd64 \
    && chmod +x skaffold \
    && mv skaffold /usr/local/bin

WORKDIR /config

CMD ["sleep", "infinity"]
